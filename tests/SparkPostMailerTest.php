<?php

use Gam6itko\MailerBundle\Mailer\SparkPostMailer;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Gam6itko\MailerBundle\Mailer\SparkPostMailer
 */
class SparkPostMailerTest extends TestCase
{
    public function testSend()
    {
        $mailer = new SparkPostMailer($_SERVER['API_KEY'], true);
        $res = $mailer
            ->addMailTo('nobody@mail.ru', 'Nobody')
            ->setSubject('api test')
            ->setFrom($_SERVER['SEND_FROM'], 'Gam6itko')
            ->setText('email text')
            ->setHtml('<html><body><h1>email html</h1><p>big text</p></body></html>')
            ->send();
        self::assertTrue($res);
    }
}