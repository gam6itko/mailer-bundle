<?php
namespace Gam6itko\MailerBundle\Exception;

/**
 * Class AbstractMailerException
 * @package Gam6itko\MailerBundle\Mailer\Exception
 */
abstract class AbstractMailerException extends \Exception
{
}