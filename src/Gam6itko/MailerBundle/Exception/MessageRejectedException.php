<?php
namespace Gam6itko\MailerBundle\Exception;

/**
 * Class UserSuppressException
 * @package Gam6itko\MailerBundle\Mailer\Exception
 */
class MessageRejectedException extends AbstractMailerException
{
}