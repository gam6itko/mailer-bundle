<?php
namespace Gam6itko\MailerBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class Gam6itkoMailerBundle
 * @package Gam6itko\MailerBundle
 */
class Gam6itkoMailerBundle extends Bundle
{
}