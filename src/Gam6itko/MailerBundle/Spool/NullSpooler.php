<?php
namespace Gam6itko\MailerBundle\Spool;

class NullSpooler implements SpoolerInterface
{
    public function spool(string $email, array $data)
    {
        // void
    }
}