<?php
namespace Gam6itko\MailerBundle\Mailer;

class NullSpooler implements SpoolerInterface
{
    public function spool(string $email, array $data)
    {
        // void
    }
}