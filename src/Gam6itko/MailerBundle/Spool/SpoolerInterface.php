<?php
namespace Gam6itko\MailerBundle\Mailer;

interface SpoolerInterface
{
    public function spool(string $email, array $data);
}