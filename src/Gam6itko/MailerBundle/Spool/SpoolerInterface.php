<?php
namespace Gam6itko\MailerBundle\Spool;

interface SpoolerInterface
{
    public function spool(string $email, array $data);
}