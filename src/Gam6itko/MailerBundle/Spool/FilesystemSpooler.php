<?php
namespace Gam6itko\MailerBundle\Mailer;

/**
 * Class SpoolMailer
 * @package Gam6itko\MailerBundle\Mailer
 */
class FilesystemSpooler implements SpoolerInterface
{
    const TYPE_DISABLE = 0;
    const TYPE_ALL = 1;
    const TYPE_ERROR_ONLY = 2;

    /** @var int */
    protected $spoolType = 0;

    /** @var string */
    protected $spoolDir;

    /**
     * SpoolMailer constructor.
     * @param $spoolType
     * @param $spoolDir
     */
    public function __construct(int $spoolType, string $spoolDir = null)
    {
        $this->spoolType = $spoolType;
        $this->spoolDir = $spoolDir ?? sys_get_temp_dir();
    }

    /**
     * @return int
     */
    public function getSpoolType(): int
    {
        return $this->spoolType;
    }

    /**
     * @param $email
     * @param array $data
     * @return bool|string - spoolpath or false
     */
    public function spool(string $email, array $data)
    {
        $filePath = $this->makeFilepath($email);
        $putResult = file_put_contents($filePath, json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
        return $putResult === false ? false : $filePath;
    }

    /**
     * @param $email
     * @return string
     */
    private function makeFilepath(string $email): string
    {
        $dt = new \DateTime();
        $dir = implode(DIRECTORY_SEPARATOR, [
            $this->spoolDir,
            $dt->format('Y'),
            $dt->format('m'),
            $dt->format('d'),
            $email
        ]);

        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }

        return $dir . DIRECTORY_SEPARATOR . "{$dt->format("His")}.json";
    }
}