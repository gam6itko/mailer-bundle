<?php
namespace Gam6itko\MailerBundle\Mailer;

use Gam6itko\MailerBundle\Exception\AbstractMailerException;
use Gam6itko\MailerBundle\Exception\MessageRejectedException;
use GuzzleHttp\Client;
use Ivory\HttpAdapter\Guzzle6HttpAdapter;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;
use SparkPost\APIResponseException;
use SparkPost\SparkPost;

/**
 * Class SparkPostMailer
 * @package Gam6itko\MailerBundle\Mailer
 * @see https://developers.sparkpost.com/api/transmissions.html
 */
class SparkPostMailer implements MailerInterface
{
    use LoggerAwareTrait;

    const TEST_POSTFIX = ".sink.sparkpostmail.com";

    /** @var FilesystemSpooler */
    protected $spooler;

    /** @var bool */
    protected $testMode;

    /** @var SparkPost */
    protected $sparky;

    /** @var array данные, которые мы пошлем SparkPost */
    protected $sendData = [];

    /**
     * SparkPostMailer constructor.
     * @param string $apiKey
     * @param $testMode
     */
    public function __construct(string $apiKey, bool $testMode)
    {
        $this->testMode = $testMode;

        $httpAdapter = new Guzzle6HttpAdapter(new Client());
        $this->sparky = new SparkPost($httpAdapter, ['key' => $apiKey]);
        $this->logger = new NullLogger();
    }

    /**
     * @inheritdoc
     */
    public function setSpooler(FilesystemSpooler $spooler)
    {
        $this->spooler = $spooler;
    }

    /**
     * @inheritdoc
     */
    public function setSubject(string $subject)
    {
        $this->sendData['subject'] = $subject;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function addMailTo(string $email, string $name = '', array $additonal = [])
    {
        $address = [
            'email' => $email . ($this->testMode ? self::TEST_POSTFIX : ""),
            'name'  => $name
        ];

        $this->sendData['recipients'][] = ['address' => array_filter($address)];

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setFrom(string $email, string $name = '')
    {
        $from = ['email' => $email, 'name' => $name];
        $this->sendData["from"] = array_filter($from);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setTemplate(string $templateName, array $substitutionData = [])
    {
        // добавляем метку времени, чтобы substitutionData не был пустым, иначе sparkPost ругается
        $substitutionData['_timestamp'] = time();

        $this->sendData['template'] = $templateName;
        $this->sendData['substitutionData'] = $substitutionData;
        $this->sendData['useDraftTemplate'] = $this->testMode;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setHtml(string $html)
    {
        $this->sendData['html'] = $html;
//        if (empty($this->sendData['text'])) {
        //todo $this->setHtml($html);
//        }

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setText(string $text)
    {
        $this->sendData['text'] = $text;
        if (empty($this->sendData['html'])) {
            $this->setHtml("<html><body><pre>$text</pre></body></html>");
        }

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function send()
    {
        try {
            $result = $this->sparky->transmission->send($this->sendData);
            $this->logger->debug('SparkPostMailer:sendResult', $result);
            $this->doSpool(false);
            return true;
        } catch (APIResponseException $exc) {
            $this->doSpool(true);
            $this->logger->error($exc->getMessage(), [
                'apiCode'        => $exc->getAPICode(),
                'apiMessage'     => $exc->getAPIMessage(),
                'apiDescription' => $exc->getAPIDescription(),
                'sendData'       => $this->sendData
            ]);
            $this->adaptException($exc);

            return false;
        } finally {
            $this->wipe();
        }
    }

    public function wipe()
    {
        $this->sendData = [];
    }

    /**
     * Adapt messages
     * @param APIResponseException $exc
     * @throws AbstractMailerException
     * @see https://support.sparkpost.com/customer/portal/articles/2140916
     */
    private function adaptException(APIResponseException $exc): void
    {
        switch ($exc->getAPICode()) {
            case 1902:
                throw new MessageRejectedException($exc->getAPIDescription(), 0, $exc);
        }
    }

    /**
     * @param $isError
     */
    private function doSpool(bool $isError): void
    {
        if (null === $this->spooler) {
            return;
        }

        $type = $this->spooler->getSpoolType();
        if ($type === FilesystemSpooler::TYPE_DISABLE || (!$isError && $type === FilesystemSpooler::TYPE_ERROR_ONLY)) {
            return null;
        }

        foreach ($this->sendData['recipients'] as $r) {
            $this->spooler->spool($r['address']['email'], $this->sendData);
        }
    }
}