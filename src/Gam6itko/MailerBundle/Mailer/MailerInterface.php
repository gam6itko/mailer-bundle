<?php
namespace Gam6itko\MailerBundle\Mailer;

use Gam6itko\MailerBundle\Exception\AbstractMailerException;
use Gam6itko\MailerBundle\Spool\SpoolerInterface;

/**
 * Interface MailerInterface
 * @package Gam6itko\MailerBundle\Mailer
 */
interface MailerInterface
{
    /**
     * @param $email
     * @param string $name
     * @param array $additonal
     * @return self
     */
    public function addMailTo(string $email, string $name = '', array $additonal = []): MailerInterface;

    /**
     * @param $email
     * @param string $name
     * @return self
     */
    public function setFrom(string $email, string $name = ''): MailerInterface;

    /**
     * Set the subject of the message.
     *
     * @param string $subject
     * @return self
     */
    public function setSubject(string $subject): MailerInterface;

    /**
     * @param $templateName
     * @param array $substitutionData
     * @return self
     */
    public function setTemplate(string $templateName, array $substitutionData = []): MailerInterface;

    /**
     * @param $html
     * @return self
     */
    public function setHtml(string $html): MailerInterface;

    /**
     * @param $text
     * @return self
     */
    public function setText(string $text): MailerInterface;

    /**
     * Sends transmission
     *
     * @return boolean - success
     * @throws AbstractMailerException
     */
    public function send();

    /**
     * Clear all inner data
     * @return self
     */
    public function wipe();

    /**
     * @param SpoolerInterface $spooler
     */
    public function setSpooler(SpoolerInterface $spooler);
}