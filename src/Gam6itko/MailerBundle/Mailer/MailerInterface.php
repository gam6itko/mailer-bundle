<?php
namespace Gam6itko\MailerBundle\Mailer;

use Gam6itko\MailerBundle\Exception\AbstractMailerException;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Interface MailerInterface
 * @package Gam6itko\MailerBundle\Mailer
 */
interface MailerInterface
{
    /**
     * @param $email
     * @param string $name
     * @param array $additonal
     * @return self
     */
    public function addMailTo(string $email, string $name = '', array $additonal = []);

    /**
     * @param $email
     * @param string $name
     * @return self
     */
    public function setFrom(string $email, string $name = '');
    
    /**
     * Set the subject of the message.
     *
     * @param string $subject
     * @return self
     */
    public function setSubject(string $subject);

    /**
     * @param $templateName
     * @param array $substitutionData
     * @return self
     */
    public function setTemplate(string $templateName, array $substitutionData = []);
    
    /**
     * @param $html
     * @return self
     */
    public function setHtml(string $html);

    /**
     * @param $text
     * @return self
     */
    public function setText(string $text);
    
    /**
     * Sends transmission
     * 
     * @return boolean - success
     * @throws AbstractMailerException
     */
    public function send();

    /**
     * Clear all inner data
     * @return mixed
     */
    public function wipe();

    /**
     * @param FilesystemSpooler $spooler
     */
    public function setSpooler(FilesystemSpooler $spooler);
}