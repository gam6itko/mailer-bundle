<?php
namespace Gam6itko\MailerBundle\Mailer;

use Gam6itko\MailerBundle\Spool\SpoolerInterface;

/**
 * Decorator pattern
 */
class TraceableMailer implements MailerInterface
{
    /** @var array */
    private $trace = [];

    /** @var array */
    private $sendData;

    /** @var MailerInterface */
    private $mailer;

    /**
     * TracingMailer constructor.
     * @param MailerInterface $mailer
     */
    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @return array
     */
    public function getTrace(): array
    {
        return $this->trace;
    }

    /**
     * @inheritdoc
     */
    public function send()
    {
        $this->mailer->send();

        $this->trace[] = $this->sendData;
        $this->wipe();
    }

    /**
     * @param SpoolerInterface $spooler
     */
    public function setSpooler(SpoolerInterface $spooler)
    {
        $this->mailer->setSpooler($spooler);
    }

    /**
     * @inheritdoc
     */
    public function addMailTo(string $email, string $name = '', array $additonal = []): MailerInterface
    {
        $address = [
            'email' => $email,
            'name'  => $name
        ];

        $this->sendData['recipients'][] = ['address' => array_filter($address)];

        $this->mailer->addMailTo($email, $name, $additonal);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setFrom(string $email, string $name = ''): MailerInterface
    {
        $from = ['email' => $email, 'name' => $name];
        $this->sendData["from"] = array_filter($from);

        $this->mailer->setFrom($email, $name);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setSubject(string $subject): MailerInterface
    {
        $this->sendData['subject'] = $subject;

        $this->mailer->setSubject($subject);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setTemplate(string $templateName, array $substitutionData = []): MailerInterface
    {
        $this->sendData['template'] = $templateName;
        $this->sendData['substitutionData'] = $substitutionData;

        $this->mailer->setTemplate($templateName, $substitutionData);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setHtml(string $html): MailerInterface
    {
        $this->sendData['html'] = $html;

        $this->mailer->setHtml($html);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setText(string $text): MailerInterface
    {
        $this->sendData['text'] = $text;

        $this->mailer->setText($text);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function wipe()
    {
        $this->sendData = [];

        $this->mailer->wipe();

        return $this;
    }
}