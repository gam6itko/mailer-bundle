<?php
namespace Gam6itko\MailerBundle\Service;

use Gam6itko\MailerBundle\Mailer\FilesystemSpooler;
use Gam6itko\MailerBundle\Mailer\MailerInterface;
use Gam6itko\MailerBundle\Mailer\NullSpooler;
use Gam6itko\MailerBundle\Mailer\SparkPostMailer;
use Psr\Log\NullLogger;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;

/**
 * Class MailerFactoryService
 * @package Gam6itko\MailerBundle\Service
 */
class MailerFactoryService
{
    use ContainerAwareTrait;

    /** @var string */
    private $mailerEngine;

    /** @var array */
    private $mailerOptions;

    /**
     * MailerService constructor.
     * @param $mailerEngine
     * @param array $mailerOptions
     * @throws \Exception
     */
    public function __construct(string $mailerEngine, array $mailerOptions = [])
    {
        $nameConverter = new CamelCaseToSnakeCaseNameConverter();
        $entityName = ucfirst($nameConverter->denormalize($mailerEngine));
        $this->mailerEngine = $entityName;
        $this->mailerOptions = $mailerOptions;
    }

    /**
     * @return MailerInterface
     */
    public function newInstance()
    {
        $mailer = null;
        switch ($this->mailerEngine) {
            case 'spark_post':
                $mailer = new SparkPostMailer($this->mailerOptions['credentials']['api_key'], $this->mailerOptions['test_mode']);
                break;
        }
        if ($this->mailerOptions['spool']) {
            switch ($this->mailerOptions['spool']['type']) {
                case 'filesystem':
                    list($t, $d) = $this->mailerOptions['spool']['args'];
                    $mailer->setSpooler(new FilesystemSpooler($t, $d));
                    break;

                case 'null':
                    $mailer->setSpooler(new NullSpooler());
                    break;
            }
        }
        return $mailer;
    }
}