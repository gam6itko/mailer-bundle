<?php
namespace Gam6itko\MailerBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * @inheritdoc
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('gam6itko_mailer');

        $rootNode
            ->children()
                ->enumNode('engine')
                    ->isRequired()
                    ->values(['spark_post']) //do more realizations
                ->end()// engine
                ->arrayNode('options')
                    ->children()
                        ->arrayNode('credentials')
                            ->prototype('scalar')->end()
                        ->end()
                        ->booleanNode('test_mode')
                            ->defaultFalse()
                        ->end()
                        ->arrayNode('spool')
                            ->children()
                                ->enumNode('type')
                                    ->info('0 - not use spool;  1- spool all; 2 - spoll errors only')
                                    ->values([0, 1, 2])
                                ->end()
                                ->scalarNode('folder')->end()
                            ->end()
                        ->end()
                    ->end()
                ->end() //options
            ->end();

        return $treeBuilder;
    }
}
