```yaml
gam6itko_mailer:
    engine: spark_post
    options:
        credentials:
            api_key: "%mailer.spark_post.api_key%"
        test_mode: true
        spool: # сохранения сообщений в файлы для дальнейшего анализа
            type: 1 # [0 - не сохранять; 1 - сохранять все письма; 2 - сохранять только ошибки]
            folder: "%kernel.root_dir%/../var/spool" # в какую папку складывать
```